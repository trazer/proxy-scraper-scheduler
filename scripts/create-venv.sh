#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

VENV_NAME=proxy-scraper-scheduler

python3.7 -m venv --clear ~/.virtualenvs/${VENV_NAME}
source ~/.virtualenvs/${VENV_NAME}/bin/activate
python -m pip install --upgrade pip
pip install --upgrade setuptools
